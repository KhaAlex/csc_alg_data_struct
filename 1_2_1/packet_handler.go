package main

import (
	"fmt"
	"os"
)

//list definition
type Element struct {
	next  *Element
	Value interface{}
}

type List struct {
	*Element
	head, tail *Element
}

func (l *List) Add(e interface{}) {
	if l.head == nil {
		l.Element = new(Element)
		l.head = l.Element
		l.tail = l.Element
		l.Element.Value = e
	} else {
		l.tail.next = new(Element)
		l.tail.next.Value = e
		l.tail = l.tail.next
		l.Element = l.next
	}
}

func (l *List) Pop() interface{} {
	if l.head == nil {
		return nil
	}
	r := l.head.Value
	if l.head == l.tail {
		l.Element = nil
		l.head = nil
		l.tail = nil
	} else {
		l.head = l.head.next
	}
	return r
}

func (l *List) Next() {
	l.Element = l.Element.next
	return
}

func (l *List) Get() interface{} {
	if l.Element == nil {
		return nil
	}
	return l.Value
}

//end list definition

type Packet struct {
	time   uint
	weight uint
	htime  int
}

func main() {
	var (
		buffer_free, packets_count int
		packet                     Packet
		p                          Packet
		buffer                     List
		time                       uint
	)
	time = 0
	fmt.Fscanf(os.Stdin, "%d %d\n", &buffer_free, &packets_count)
	for packets_count > 0 || buffer.Get() != nil {
		for buffer_free > 0 && packets_count > 0 {
			fmt.Fscanf(os.Stdin, "%d %d\n", &packet.time, &packet.weight)
			packets_count--
			fmt.Println("~", packet.time, "~", time)
			if time <= packet.time {
				buffer.Add(packet)
				buffer_free--
				//				fmt.Println("~",buffer_free)
			} else {
				packet.htime = -1
				buffer.Add(packet)
			}
		}
		if buffer.Get() != nil {
			p = buffer.Pop().(Packet)
		}
		if p.htime >= 0 {
			if p.time > time {
				time = p.time
			}
			fmt.Println(time)
			time = p.weight + time
			buffer_free++
			//			fmt.Println("~",buffer_free)
		} else {
			fmt.Println(p.htime, p.time)
		}
	}
}
