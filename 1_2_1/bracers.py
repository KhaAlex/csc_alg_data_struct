bracers = input()
check = list()
opening = {')':'(',']':'[','}':'{'}
for i in range(len(bracers)):
	if bracers[i] in "([{":
		check.append(i)
	elif check and bracers[i] in ")]}":
		if opening[bracers[i]]==bracers[check[-1]]:
			check.pop()
		else:
			print(i+1)
			exit()
	elif not check and bracers[i] in ")]}":
		print(i+1)
		exit()
if check:
	print(check[0]+1)
else:
	print("Success")