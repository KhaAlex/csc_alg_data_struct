package main

import (
	"fmt"
)

//list definition
type Element struct {
	next  *Element
	Value interface{}
}

type List struct {
	*Element
	head, tail *Element
}

func (l *List) Add(e interface{}) {
	if l.head == nil {
		l.Element = new(Element)
		l.head = l.Element
		l.tail = l.Element
		l.Element.Value = e
	} else {
		l.tail.next = new(Element)
		l.tail.next.Value = e
		l.tail = l.tail.next
		l.Element = l.next
	}
}

func (l *List) Pop() interface{} {
	if l.head == nil {
		return nil
	}
	r := l.head.Value
	if l.head == l.tail {
		l.Element = nil
		l.head = nil
		l.tail = nil
	} else {
		l.head = l.head.next
	}
	return r
}

func (l *List) Next() {
	l.Element = l.Element.next
	return
}

func (l *List) Get() interface{} {
	if l.Element == nil {
		return nil
	}
	return l.Value
}

//end list definition

type Packet struct {
	time   uint
	weight int
}

func main() {
	var (
		l List
		p Packet
	)
	l.Add(10)
	l.Add("Hello")
	l.Add(Packet{2, 3})
	l.Add(Packet{3, 4})
	fmt.Println(l.Get())
	fmt.Println(l.Pop())
	fmt.Println(l.Pop())
	p = l.Pop().(Packet)
	fmt.Println(p.time, p.weight)
	p = l.Pop().(Packet)
	fmt.Println(p.time, p.weight)
}
