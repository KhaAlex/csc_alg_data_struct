import sys
sys.setrecursionlimit(20000)

class Node:
		def __init__(self):
			self.childs=list()

		def addChild(self,child):
			self.childs.append(child)

def get_depth(sub_child, depth):
	depth=1
	childs=sub_child.childs
	for child in childs:
		depth=max(depth, get_depth(child, depth)+1)
	return depth

n=int(input())
tr=dict()
root=-2
num=''
for pos in range(n) :
	char=''
	while char !=' ' and char != '\n':
		char=sys.stdin.read(1)
		num+=char
	anc=int(num)
	if pos not in tr.keys():
		tr[pos]=Node()
	if anc in tr.keys():
		tr[anc].addChild(tr[pos])
	elif int(anc)==-1:
		root=pos
	else:
		tr[anc]=Node()
		tr[anc].addChild(tr[pos])
	num=''

print(get_depth(tr[root],0))