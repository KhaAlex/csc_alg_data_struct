package main

import (
	"fmt"
)

func pr(t interface{}) {
	fmt.Println(t)
}

func main() {
	i := 10
	pr(i)
	j := "Hello"
	pr(j)

}
