import unittest
from tree_depth import *

class TreeDepthTest(unittest.TestCase):
	def test_depth(self):
		inp="""9 7 5 5 2 9 9 9 2 -1"""
		tr=Tree()
		tr.tree_build(inp)
		self.assertEqual(tr.tree_depth(),4)

	def test_depth1(self):
		inp="""4 -1 4 1 1"""
		tr=Tree()
		tr.tree_build(inp)
		self.assertEqual(tr.tree_depth(),3)
	
	def test_tree_build(self):
		inp="""9 7 5 5 2 9 9 9 2 -1"""
		tr=Tree()
		tr.tree_build(inp)
		self.assertEqual(tr.getRoot().id,9)

	def test_tree_build1(self):
		inp="""4 -1 4 1 1"""
		tr=Tree()
		tr.tree_build(inp)
		self.assertEqual(tr.getRoot().id,1)

	def test_tree_build2(self):
		inp="""2 2 3 5 5 7 7 9 9 -1"""
		tr=Tree()
		tr.tree_build(inp)
		self.assertEqual(tr.tree_depth(),6)

	def test_bad_first(self):
		n=5
		inp='''-1 0 4 0 3'''
		tr=Tree()
		tr.tree_build(inp)
		self.assertEqual(tr.tree_depth(),4)

	def test_one(self):
		n=1
		inp='''-1'''
		tr=Tree()
		tr.tree_build(inp)
		self.assertEqual(tr.tree_depth(),1)



	def test_big(self):
		num=10**5
		raw = [i+1 for i in range(num-1)] + [-1]
		inp=''
		for i in raw:
			inp+=str(i)+' '
		inp=inp.strip(' ')
		tr=Tree()
		tr.tree_build(inp)
		self.assertEqual(tr.getRoot().id,99999)
	
if __name__ == "__main__":
	unittest.main()