package main

import (
	"os"
	"fmt"
	"errors"

)



type Packet struct {
	time int
	weight uint
	dropped:=false
}

type Handler struct {
	time uint
	seq List
	buffer uint
	wait_queue_cnt uint
}

func (h *Handler) init(wait_queue_cnt int) {
	h.wait_queue_cnt = wait_queue_cnt
	seq = list.New()
}

func (h *Handler) handle(packet Packet) {
	h.seq.PushFront(packet)
}

func (h *Handler) process() {
	for h.buffer==0 {
		p:=list.Remove(seq.Back())
		if p.dropped {
			fmt.Println("-1")
		} else {
			fmt.Println(p.time)
			h.buffer+=p.weight
			h.wait_queue_cnt++
		}
	}

	for e := h.seq.Back(); e != nil; e = e.Prev() {
		switch {
			case !e.dropped && h.wait_queue_cnt > 0:
				h.wait_queue_cnt--
			case !e.dropped && h.wait_queue_cnt = 0:
				e.dropped:=true
		}
	}

	if h.buffer>0 {
		h.buffer--	
	}
	h.time++

}

func main() {

	var (
		wait_queue_size,packets_count int
		packet Packet
		handler Handler
	)

	fmt.Fscanf(os.Stdin,"%d %d\n",&wait_queue_size,&packets_count)
	handler.init(wait_queue_size)

	for i := range packets_count {
		if packet.time==handler.time {
			fmt.Fscanf(os.Stdin,"%d %d\n",&packet.time,&packet.weight)
			handler.handle(packet)
			continue
		}
		handler.process()
	}
	defer handler.process()
}
