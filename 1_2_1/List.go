package List

type Element struct {
	next *Element
	Value interface{}
}

type List struct {
	*Element
	head, tail *Element
}

func (l *List) Add(e interface{}) {
	if l.head==nil {
		l.Element=new(Element)
		l.head=l.Element
		l.tail=l.Element
		l.Element.Value:=e
	} else {
		l.tail.next:=new(Element)
		l.tail.next.Value:=e
		l.tail=l.tail.next
	}
}

func (l *List) Next() *Element {
	l.Element:=l.Element.next
	return l.Element
}