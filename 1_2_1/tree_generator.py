import random
MAX_NODES=100000

random.seed()

nodes_count=random.randint(1,MAX_NODES)
root=random.choice(range(nodes_count))
filled=list()
filled.append(root)
rng=[*range(nodes_count)]
random.shuffle(rng)
tree=dict()

print(nodes_count)

while rng:
	i=rng.pop()
	if i==root:
		tree[i]=-1
	else:
		tree[i]=random.choice(filled)
		filled.append(i)

for i, n in sorted(tree.items()):
	print(n, end=' ')
print('\n')
# add -1 to random list element and add to filled set its index
# then from 0 to n fill random.sample(filled) except -1 index