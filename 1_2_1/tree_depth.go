package main

import (
	"fmt"
	"os"
)

func max(a int, b int) int {
	if a > b {
		return a
	}
	return b
}

func get_depth(sub_child *Node, max_depth int) int {
	max_depth++
	depth := max_depth
	for _, sub_ch := range sub_child.Childs {
		max_depth = max(max_depth, get_depth(sub_ch, depth))
	}
	return max_depth
}

type Node struct {
	Anc    *Node
	Childs []*Node
}

func (n *Node) AddChild(child *Node) {
	n.Childs = append(n.Childs, child)
}

type Tree struct {
	nodes []Node
	root  *Node
}

func (t Tree) tree_depth() int {
	return get_depth(t.root, 0)
}

func main() {
	var nodes_count, content int
	fmt.Fscan(os.Stdin, &nodes_count)
	tree := Tree{make([]Node, nodes_count), nil}
	for i := 0; i < nodes_count; i++ {
		fmt.Fscan(os.Stdin, &content)
		if content > -1 {
			tree.nodes[i].Anc = &tree.nodes[content]
			tree.nodes[content].AddChild(&tree.nodes[i])
		} else {
			tree.root = &tree.nodes[i]
			tree.nodes[i].Anc = nil
		}
	}
	fmt.Println(tree.tree_depth())
}
