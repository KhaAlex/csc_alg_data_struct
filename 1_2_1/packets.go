package main

import (
	"fmt"
	"os"
)

var time int

//list definition
type Element struct {
	next  *Element
	Value interface{}
}

type List struct {
	*Element
	head, tail *Element
}

func (l *List) Add(e interface{}) {
	if l.head == nil {
		l.Element = new(Element)
		l.head = l.Element
		l.tail = l.Element
		l.Element.Value = e
	} else {
		l.tail.next = new(Element)
		l.tail.next.Value = e
		l.tail = l.tail.next
		l.Element = l.next
	}
}

func (l *List) Pop() interface{} {
	if l.head == nil {
		return nil
	}
	r := l.head.Value
	if l.head == l.tail {
		l.Element = nil
		l.head = nil
		l.tail = nil
	} else {
		l.head = l.head.next
	}
	return r
}

func (l *List) Next() {
	l.Element = l.Element.next
	return
}

func (l *List) Get() interface{} {
	if l.Element == nil {
		return nil
	}
	return l.Value
}

//end list definition

type Packet struct {
	time   int
	weight int
	htime  int
}

type processor struct {
	prc, buffer_free int
	buffer           List
}

func (pr *processor) Load(p Packet) {
	if pr.buffer_free > 0 && p.htime >= 0 {
		pr.buffer.Add(p)
		pr.buffer_free--
	} else {
		p.htime = -1
		pr.buffer.Add(p)
	}
	pr.prc = p.weight
	if p.htime >= 0 {
		p.htime = time
	}
}

func (pr *processor) Tick() {
	if pr.prc > 0 {
		pr.prc--
		time++
	}
}

func (pr *processor) tickNoTime() {
	// move zero and dropped packets
}

func (pr *processor) TickTo(t int) {
	// increase time to t while processing packets
}

func (pr *processor) IsFree() bool {
	if pr.prc == 0 {
		return true
	} else {
		return false
	}
}

func main() {
	var (
		packets_count int
		packet        Packet
		proc          processor
	)
	fmt.Fscanf(os.Stdin, "%d %d\n", &proc.buffer_free, &packets_count)
	//	takeNext:
	for i := 0; i < packets_count; i++ {
		fmt.Fscanf(os.Stdin, "%d %d\n", &packet.time, &packet.weight)
		packet.htime = 0
		proc.tickNoTime()
		if packet.time < time {
			packet.htime = -1
			proc.Load(packet)
		} else if packet.time == time {
			proc.Load(packet)
			proc.tickNoTime()
		} else {
			proc.TickTo(packet.time)
			proc.Load(packet)
		}
		if proc.IsFree() {
			continue
		} else {
			proc.Tick()
		}
	}
}

// for buffer.Get() != nil && proc.prc == 0
// fmt.Println("#", proc.prc, "~", time, "?", buffer_free, ":", p.weight)
